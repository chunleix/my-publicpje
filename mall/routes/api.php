<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace'=>'Home','middeware'=>['api','cors','response','web']],function(){
    Route::resource('/goodlist','GoodsController');
    Route::resource('/product','ProductController');
    Route::resource('/login','LoginController');
    Route::resource('/register','RegisterController');
	 Route::resource('/cart','CartController');
	 Route::resource('/tancart','TancartController');
	 Route::resource('/reducecart','ReducecartController');
	 Route::resource('/addcart','AddcartController');
	 Route::resource('/selcartgood','SelcartgoodController');
	 Route::resource('/delcart','DelcartController');
	 Route::resource('/reprovince','ReprovinceController');
	 Route::resource('/useradder','UseradderController');
	 Route::resource('/modifyuseradder','ModifyuseradderController');
	 Route::resource('/user','UserController');
	 Route::resource('/dellogin','DelloginController');
});
