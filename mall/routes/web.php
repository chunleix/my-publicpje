<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['domain'=>'admin.mall.com','namespace'=>'Admin'],function(){
    Route::resource('/login','LoginController');
    Route::group(['middleware'=>['AdminMiddleware']],function(){
        Route::resource('/','IndexController');
        Route::resource('/user','UserController');
        Route::resource('/user/store','UserController');
        Route::resource('/user/list','UserController');
    });
});
