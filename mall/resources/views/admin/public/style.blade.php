<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" >
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="{{ URL::asset('css/style.css') }}" rel='stylesheet' type='text/css' />
<link href="{{ URL::asset('css/style-responsive.css') }}" rel="stylesheet"/>
<!-- font CSS -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- font-awesome icons -->
<link rel="stylesheet" href="{{ URL::asset('css/font.css') }} " type="text/css"/>
<link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('css/morris.css') }}" type="text/css"/>
<!-- calendar -->
<link rel="stylesheet" href="{{ URL::asset('css/monthly.css') }}">