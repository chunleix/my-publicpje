<!DOCTYPE html>
<head>
    <title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    @include('admin.public.topscript')
    <!-- bootstrap-css -->
   @include('admin.public.style')
    <!-- //calendar -->
    <!-- //font-awesome icons -->

</head>
<body>
<section id="container">
    <!--header start-->
    @include('admin.public.header');
    <!--header end-->
    <!--sidebar start-->
    @include('admin.public.aside');
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- //market-->
            @yield('content')
        </section>
        <!-- footer -->
        <div class="footer">
            <div class="wthree-copyright">
                <p>Copyright &copy; 2017.Company name All rights reserved.<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a></p>
            </div>
        </div>
        <!-- / footer -->
    </section>
    <!--main content end-->
</section>
@include('admin.public.bottomscript')
</body>
</html>
