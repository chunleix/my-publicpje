@extends('admin.layouts.masters')
@section('content')
    <section class="wrapper">
        <div class="table-agile-info">
            <div class="panel panel-default">
                <div class="panel-heading">
                    用户列表
                </div>
                <div>
                    <table class="table" ui-jq="footable" ui-options='{
        "paging": {
          "enabled": true
        },
        "filtering": {
          "enabled": true
        },
        "sorting": {
          "enabled": true
        }}'>
                        <thead>
                        <tr>
                            <th data-breakpoints="xs">ID</th>
                            <th>用户名</th>
                            <th>手机号</th>
                            <th data-breakpoints="xs">状态</th>
                            <th data-breakpoints="xs sm md" data-title="DOB">注册时间</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $k=>$v)
                        <tr data-expanded="true">
                            <td>{{$k+1}}</td>
                            <td>{{$v->username}}</td>
                            <td>{{$v->tel}}</td>
                            @if($v->status == 1)
                                <td>启用</td>
                                @else
                                <td>禁用</td>
                                @endif

                            <td><?php echo date('Y-m-d H:i:s',$v->addtime); ?></td>
                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection