@extends('admin.layouts.masters')
@section('content')
    <header class="panel-heading">
       添加用户
    </header>
    <div class="panel-body">
        <div class="position-center">
            <form role="form" action="{{ url('/user/store') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputEmail1">用户名</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="username">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">手机号</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="tel">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">密码</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <input type="file" id="exampleInputFile" name="pic">
                    <p class="help-block">Example block-level help text here.</p>
                </div>

                <button type="submit" class="btn btn-info">保存</button>
            </form>
        </div>

    </div>
@endsection