<?php
namespace App\Store;

use DB;
class AddcartStore
{
    private static $table = 'data_cart';

    public function addcart($good_id,$username)
    {
	
			$reuserid = DB::select("select id from data_users where username=?",[$username]);
			foreach($reuserid as $userid){
				$userid = $userid->id;
			}
			
			$carts = DB::select("select * from data_cart where guid=? and good_id=?",[$userid,$good_id]);
			foreach($carts as $cart){
				$num = $cart->num;
			}
			if($num>=9){
				$num = 9;
			} else {
				$num = $num+1;
			}
			
			$result = DB::update("update data_cart set num=? where guid=? and good_id=?",[$num,$userid,$good_id]);
			if($result){
				 
				$data = array(
					'status'=>'ok',
					'msg'=>'成功',
				);
				return $data;
			}
			
    }

}