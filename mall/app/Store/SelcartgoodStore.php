<?php
namespace App\Store;

use DB;
class SelcartgoodStore
{
    private static $table = 'data_cart';

    public function selcart($good_id,$username,$num)
    {
	
			$reuserid = DB::select("select id from data_users where username=?",[$username]);
			foreach($reuserid as $userid){
				$userid = $userid->id;
			}
			
			$result = DB::update("update data_cart set is_sel=? where guid=? and good_id=?",[$num,$userid,$good_id]);
			if($result){
				$data = array(
					'status'=>'ok',
					'msg'=>'成功',
				);
			}
			return $data;
    }
	public function allsel($username,$num){
		$reuserid = DB::select("select id from data_users where username=?",[$username]);
		foreach($reuserid as $userid){
			$userid = $userid->id;
		}
		$result = DB::update("update data_cart set is_sel=? where guid=? ",[$num,$userid]);
		if($result){
			$data = array(
				'status'=>'ok',
				'msg'=>'成功',
			);
		}
		return $data;
	}

}