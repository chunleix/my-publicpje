<?php
namespace App\Store;

class AdminStore
{
    public static function getAdminLogin($where)
    {
        return \DB::table('data_admin')->where($where)->first();
    }
}