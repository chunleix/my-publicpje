<?php
namespace App\Store;

use DB;
class TancartStore
{
    private static $table = 'data_cart';

    public function obcart($guid)
    {
		
		$user = DB::select("select * from data_users where username = ?",[$guid]);
		
		foreach($user as $users){
			$guid = $users->id;
		}
		
		$carts = DB::select('select * from data_cart where guid = ? ', [$guid]);
		foreach($carts as $cart){
			$goodid = $cart->good_id;
			$goods = DB::select("select * from data_goods where guid = ?",[$goodid]);
			
			$data[] = array(
				'good_id'=>$goods[0]->guid,
				'title'=>$goods[0]->title,
				'price'=>$goods[0]->price,
				'price'=>$goods[0]->price,
				'num'=>$cart->num,
				'is_sel'=>$cart->is_sel,
			);
		}
		
		return $data;
    }

}