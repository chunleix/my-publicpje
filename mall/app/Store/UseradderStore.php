<?php
namespace App\Store;

use DB;
class UseradderStore
{

    public function useradder($data,$username)
    {
			//根据用户名查找用户id
			$reuserid = DB::select("select id from data_users where username=?",[$username]);
			
			foreach($reuserid as $userid){
				$userid = $userid->id;
			}
			
			//根据用户id确认此用户是否已经存在地址，如果不存在则将现在这个地址设置为默认地址
			$cart = DB::select("select id from mypro_user_addr where user_id=?",[$userid]);
			if(empty($cart)){
				$is_default = 1;
			} else {
				$is_default = 0;
			}
			$time = time();
			
			//$sql = "INSERT INTO mypro_user_addr(user_id,province_code,city_code,area_code,town_code,detailed_addr,addr_name,addr_phone,is_default,time) VALUES('{$userid}','{$data['province_code']}','{$data['city_code']}','{$data['area_code']}','{$data['town_code']}','{$data['detailed_addr']}','{$data['name']}','{$data['addr_phone']}','{$is_default}','{$time}')";
			
	$result = DB::insert("INSERT INTO mypro_user_addr(user_id,province_code,city_code,area_code,town_code,detailed_addr,addr_name,addr_phone,is_default,time) VALUES('{$userid}','{$data['province_code']}','{$data['city_code']}','{$data['area_code']}','{$data['town_code']}','{$data['detailed_addr']}','{$data['name']}','{$data['addr_phone']}','{$is_default}','{$time}')");
			if($result){
				$data = array(
					'status'=>'ok',
					'msg'=>'添加成功',
				);
			}
			return $data;
			
    }

	//查找用户地址列表
	public function adderall($username){
			//根据用户名查找用户id
			$reuserid = DB::select("select id from data_users where username=?",[$username]);
			foreach($reuserid as $userid){
				$userid = $userid->id;
			}
			$result = DB::select("select id,addr_name,addr_phone,detailed_addr,is_default from mypro_user_addr where user_id=? order by is_default",[$userid]);
			foreach($result as $results){
				$data[] = array(
					'id'=>$results->id,
					'addr_name'=>$results->addr_name,
					'addr_phone'=>$results->addr_phone,
					'detailed_addr'=>$results->detailed_addr,
					'is_default'=>$results->is_default,
				);
			}
			return $data;
	}
	
	//查找地址
	public function seladder($cart_id){
		
		$cart = DB::select("select * from mypro_user_addr where id=?",[$cart_id]);
		foreach($cart as $carts){
			$data = array(
				'id'=>$carts->id,
				'user_id'=>$carts->user_id,
				'province_code'=>$carts->province_code,
				'city_code'=>$carts->city_code,
				'area_code'=>$carts->area_code,
				'town_code'=>$carts->town_code,
				'detailed_addr'=>$carts->detailed_addr,
				'addr_name'=>$carts->addr_name,
				'addr_phone'=>$carts->addr_phone,
				'is_default'=>$carts->is_default,
			);
		}
		return $data;
	}
	
}