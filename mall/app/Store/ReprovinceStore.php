<?php
namespace App\Store;

class ReprovinceStore
{
    private static $table = 'data_address';
	//查找省份
    public function province()
    {
		//获取所有省份
		return\DB::table(self::$table)->select('*')->where('parentId','=',0)->get();
		
    }
	
	//查找省份对应的城市
	public function city($province_id){
		return\DB::table(self::$table)->select('*')->where('parentId','=',$province_id)->get();
	}
	
	//查找城市对应的县/镇
	public function county($city_id){
		return\DB::table(self::$table)->select('*')->where('parentId','=',$city_id)->get();
	}
	
	//查找县/镇对应的街道
	public function street($county_id){
		return\DB::table(self::$table)->select('*')->where('parentId','=',$county_id)->get();
	}
}