<?php
namespace App\Store;

use DB;
class DelcartStore
{
    private static $table = 'data_cart';

    public function delcart($good_id,$username)
    {
	
			$reuserid = DB::select("select id from data_users where username=?",[$username]);
			foreach($reuserid as $userid){
				$userid = $userid->id;
			}
			
			$result = DB::delete('delete from data_cart where guid=? and good_id=?',[$userid,$good_id]);
			if($result){
				$data = array(
					'status'=>'ok',
					'msg'=>'删除成功',
				);
			}
			return $data;
    }

}