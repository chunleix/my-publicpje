<?php
namespace App\Store;
use Redis;
class GoodsStore
{
    private static $table = 'data_goods';
    public $goodlist = STRING_GOOD_LIST_;   //商品列表缓存 后接页数
    public $idlist = LIST_GOOD_ID_LIST;     //商品id列表
    public $goodinfo= HASH_GOOD_INFO_;      //商品详情缓存 后接商品id
    public function getgoodlist()
    {
        return \DB::table('data_goods')->get();
    }
    /**
     *   获取商品列表 api
     *   @param $num 传入的页数 1     $page 每页显示条数
     *   @page  这里自定义五条数据分一页
     *   @return mixed
     */
    public function apiGoodsList($num)
    {  //Redis::flushAll();die;
        $num = ($num-1) * 8;
        $upage = ($num-1) + 8;
        $lpush = Redis::lRange($this->idlist,$num,$upage);

        if(empty($lpush)){
            $goodlist = \DB::table(self::$table)->get();
            foreach($goodlist as $key=>$value){
                Redis::rpush($this->idlist,$value->guid);
                $goodinfo = $this->goodinfo.$value->guid;
                $goods = \DB::table(self::$table)->where('guid','=',$value->guid)->get();
                $goodsall[] = $goods[0];
                Redis::hmset($goodinfo,(array)$goods[0]);
            }
            return array_slice($goodsall,0,$upage);
        } else {
            foreach($lpush as $hash_k=>$hash_v){
                    $hashall = $this->goodinfo.$hash_v;
                    $hashinfo[] = Redis::hgetall($hashall);
            }
            return $hashinfo;
        }
    }
}