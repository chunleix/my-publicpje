<?php
namespace App\Service;

use App\Store\SelcartgoodStore;
use App\Tools\Common;

class SelcartgoodService
{
    private static $selcartgoodStore;
    public function __construct(SelcartgoodStore $selcartgoodStore)
    {
        self::$selcartgoodStore = $selcartgoodStore;
    }
    public function selcart($good_id,$username,$num){
		
        $result = self::$selcartgoodStore->selcart($good_id,$username,$num);
        return $result;
    }

	public function allsel($username,$num){
		
        $result = self::$selcartgoodStore->allsel($username,$num);
        return $result;
    }
	
}
