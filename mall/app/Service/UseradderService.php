<?php
namespace App\Service;

use App\Tools\Common;
use App\Store\UseradderStore;
class UseradderService
{
    private static $useradderStore;
    public function __construct(UseradderStore $useradderStore)
    {
        self::$useradderStore = $useradderStore;
    }

    public function useradder($data,$username)
    {
		
		$result = self::$useradderStore->useradder($data,$username);
		return $result;
    }
	
	public function adderall($username)
    {
		
		$result = self::$useradderStore->adderall($username);
		return $result;
    }
	public function seladder($cart_id)
    {
		
		$result = self::$useradderStore->seladder($cart_id);
		return $result;
    }

}