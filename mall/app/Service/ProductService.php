<?php
namespace App\Service;

use App\Store\ProductStore;
use App\Tools\Common;

class ProductService
{
    private static $productStore;
    public function __construct(ProductStore $productStore)
    {
        self::$productStore = $productStore;
    }
    public function getproduct($id){
		
        $result = self::$productStore->getgoodlist($id);
        return $result;
    }

}
