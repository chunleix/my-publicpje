<?php
namespace App\Service;

use App\Store\GoodsStore;
use App\Tools\Common;

class GoodsService
{
    private static $goodsStore;
    public function __construct(GoodsStore $goodsStore)
    {
        self::$goodsStore = $goodsStore;
    }
    public function getgoodlist(){
        $result = self::$goodsStore->getgoodlist();
        return $result;
    }
    public function apigoodslist($num){
        return self::$goodsStore->apiGoodsList($num);
    }
}
