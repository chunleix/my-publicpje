<?php
namespace App\Service;

use App\Store\AddcartStore;
use App\Tools\Common;

class AddcartService
{
    private static $addcartStore;
    public function __construct(AddcartStore $addcartStore)
    {
        self::$addcartStore = $addcartStore;
    }
    public function addcart($good_id,$username){
		
        $result = self::$addcartStore->addcart($good_id,$username);
        return $result;
    }

}
