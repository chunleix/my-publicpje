<?php
namespace App\Service;

use App\Store\ReducecartStore;
use App\Tools\Common;

class ReducecartService
{
    private static $reducecartStore;
    public function __construct(ReducecartStore $reducecartStore)
    {
        self::$reducecartStore = $reducecartStore;
    }
    public function recart($good_id,$username){
		
        $result = self::$reducecartStore->recart($good_id,$username);
        return $result;
    }

}
