<?php
namespace App\Service;

use App\Store\RegisterStore;
use App\Tools\Common;
use Illuminate\Contracts\Encryption\DecryptException;
use Crypt;

class RegisterService
{
    private static $registerStore;
    public function __construct(RegisterStore $registerStore)
    {
        self::$registerStore = $registerStore;
    }
    public function doregister($data){
		$pswd_open = Crypt::encrypt($data['userpass']);
		$data = array(
			'userpass'=>$pswd_open,
			'username'=>$data['username'],
		);
		
        $result = self::$registerStore->doregister($data);
		return $result;
        
    }

}
