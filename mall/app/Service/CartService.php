<?php
namespace App\Service;

use App\Store\cartStore;
use App\Tools\Common;

class CartService
{
    private static $cartStore;
    public function __construct(cartStore $cartStore)
    {
        self::$cartStore = $cartStore;
    }
    public function addCartSer($guid,$sessions){
		
        $result = self::$cartStore->addCartSto($guid,$sessions);
        return $result;
    }

}
