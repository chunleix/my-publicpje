<?php
namespace App\Service;

use App\Store\DelcartStore;
use App\Tools\Common;

class DelcartService
{
    private static $delcartStore;
    public function __construct(DelcartStore $delcartStore)
    {
        self::$delcartStore = $delcartStore;
    }
    public function delcart($good_id,$username){
		
        $result = self::$delcartStore->delcart($good_id,$username);
        return $result;
    }

}
