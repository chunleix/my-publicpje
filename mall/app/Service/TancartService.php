<?php
namespace App\Service;

use App\Store\TancartStore;
use App\Tools\Common;

class TancartService
{
    private static $tancartStore;
    public function __construct(TancartStore $tancartStore)
    {
        self::$tancartStore = $tancartStore;
    }
    public function obcart($guid){
		
        $result = self::$tancartStore->obcart($guid);
        return $result;
    }

}
