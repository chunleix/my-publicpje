<?php
namespace App\Service;

use App\Tools\Common;
use App\Store\UserStore;
class UserService
{
    private static $UserStore;
    public function __construct(UserStore $userStore)
    {
        self::$UserStore = $userStore;
    }

    public function create($data)
    {
        $password = Common::cryptString($data['password']);
        $uuid = Common::getUuid();
        $param = [
            'guid'=>$uuid,
            'username'=>$data['username'],
            'tel'=>$data['tel'],
            'password'=>$password,
            'pic'=>'1.png',
            'addtime'=>time(),
        ];
        return self::$UserStore->where($param);
    }

    public function datall()
    {
        return self::$UserStore->select();
    }
}