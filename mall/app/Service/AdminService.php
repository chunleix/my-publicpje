<?php
namespace App\Service;

use App\Store\AdminStore;
use App\Tools\Common;
use Illuminate\Support\Facades\Session;

class AdminService
{
    private static $adminStore;
    public function __construct(AdminStore $adminStore)
    {
        self::$adminStore = $adminStore;
    }
    public function login($data)
    {
        $password = Common::cryptString($data['password']);
        $where = [
            'username'=>$data['username'],
            'password'=>$password,
            'status'=>1,
        ];

        $result = self::$adminStore->getAdminLogin($where);
        if(!empty($result)){
            Session::put('admin',$result);
        }
        return $result;
    }
}
