<?php
namespace App\Service;

use App\Store\ReprovinceStore;
use App\Tools\Common;

class ReprovinceService
{
    private static $reprovinceStore;
    public function __construct(ReprovinceStore $reprovinceStore)
    {
        self::$reprovinceStore = $reprovinceStore;
    }
	//查找省份
    public function province(){
		
        $result = self::$reprovinceStore->province();
        return $result;
    }
	//查找省份对应的城市
	public function city($province_id){
		 $result = self::$reprovinceStore->city($province_id);
        return $result;
	}
	//查找城市对应的县/镇
	public function county($city_id){
		 $result = self::$reprovinceStore->county($city_id);
        return $result;
	}
	//查找县/镇对应的街道
	public function street($county_id){
		 $result = self::$reprovinceStore->county($county_id);
        return $result;
	}
}
