<?php
namespace App\Service;

use App\Store\LoginStore;
use App\Tools\Common;
use Illuminate\Contracts\Encryption\DecryptException;
use Crypt;
class LoginService
{
    private static $loginStore;
    public function __construct(LoginStore $loginStore)
    {
        self::$loginStore = $loginStore;
    }
    public function dologin($username){
		
        $result = self::$loginStore->dologin($username);
		
		if(empty($result)){
			$data = array(
				'status'=>'no',
				'msg'=>'账号不存在',
			);
			return $data;
		} else {
			$data = array(
				'status'=>'ok',
				'msg'=>'用户名密码正确',
			);
			\Redis::Setex('username',7200,$username);
			return $data;
			/* $decrypted = Crypt::decrypt($userpass);
			print_r($decrypted);die; */
		}
        
    }
	public function repass($username,$userpass){
		
		$result = self::$loginStore->repass($username);
		
		if($result['status'] == 'no'){
			$data = array(
				'status'=>'no',
				'msg'=>'用户没有注册',
			);
			return $data;
		}
		
		$decrypted = Crypt::decrypt($result['msg']);
		
		
		if($userpass == $decrypted){
			$data = array(
				'status'=>'ok',
				'msg'=>'密码正确',
			);
			return $data;
		} else {
			
			$data = array(
				'status'=>'no',
				'msg'=>'密码错误',
			);
			return $data;
		}
	}

}
