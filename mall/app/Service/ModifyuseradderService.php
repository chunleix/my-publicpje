<?php
namespace App\Service;

use App\Store\ModifyuseradderStore;
use App\Tools\Common;

class ModifyuseradderService
{
    private static $modifyuseradderService;
    public function __construct(ModifyuseradderService $modifyuseradderService)
    {
        self::$modifyuseradderService = $modifyuseradderService;
    }
    public function modifyadder($data){
		
        $result = self::$modifyuseradderService->modifyadder($data);
        return $result;
    }

}
