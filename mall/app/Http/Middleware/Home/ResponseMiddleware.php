<?php

namespace App\Http\Middleware\Home;

use Closure;

class ResponseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $data = $next($request);
        return $data;
        // $result = $data->getStatusCode();

    }
}
