<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Service\ReprovinceService;
class ReprovinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	private  $reprovinceService;
	public function __construct(ReprovinceService $reprovinceService)
    {
       $this->reprovinceService = $reprovinceService;
    }
	 

    public function index(Request $request,Response $response)
    {	
		//获取省份id
		$province_id = $request->get('province_id');

		//获取城市id
		$city_id = $request->get('city_id'); 
		//获取城镇，县id
		$county_id = $request->get('county_id'); 
		//如果传入了省份id，则去查找对应城市
		if($province_id){
			
			$city = $this->reprovinceService->city($province_id);
			return $city;
			//如果传入城市id，则去查找县
		} else if($city_id){
			
			$city = $this->reprovinceService->county($city_id);
			return $city;
			
			
			//如果传入县id，则取查找街道
		} else if($county_id){
			$street = $this->reprovinceService->street($county_id);
			return $street;
			//如果没有传入，则要查找省份
		} else {
			$product = $this->reprovinceService->province();
			return $product;
		}
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
