<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\RegisterService;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Response;


class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	private  $registerService;
    public function __construct(RegisterService $registerService)
    {
       $this->registerService = $registerService;
    }
	 
    public function index(Request $request,Response $response)
    {
		
		$username = $request->get('name');
		$userpass = $request->get('pass');
		$usercode = $request->get('upass');
		if(strlen($userpass) > 18 || strlen($userpass) < 6){
			$result = array(
				'status'=>'2',
				'msg'=>'密码长度不正确',
			);
			return $result;
		}
		if(!preg_match("/^1[34578]{1}\d{9}$/",$username)){
			$result = array(
				'status'=>'2',
				'msg'=>'请输入正确手机号',
			);
			return $result;
		}
		if($userpass == $usercode){
			$data = array(
				'username'=>$username,
				'userpass'=>$userpass
			);
			$result = $this->registerService->doregister($data);
			if($result == 'ok'){
				\Redis::Setex('username',3600,$username);
			}
			return $result;
		} else {
			$result = array(
				'status'=>'2',
				'msg'=>'两次输入密码不一致',
			);
			return $result;
		}
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
