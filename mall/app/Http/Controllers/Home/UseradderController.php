<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\UseradderService;
use Symfony\Component\HttpFoundation\Response;
use Redis;

class UseradderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	private  $useradderService;
    public function __construct(UseradderService $useradderService)
    {
       $this->useradderService = $useradderService;
    }
	 
    public function index(Request $request,Response $response)
    {
		if(!empty($request->get('cart_id'))){
			$cart_id = $request->get('cart_id');
			
			$result = $this->useradderService->seladder($cart_id);
			return $result;
		}
		
		if(empty($request->all())){
			return ;
		}
		$username = \Redis::get('username');
		if(empty($username)){
			return 0;
		}
		if(empty($request->get('is_sel'))){
			$data = $request->all();
			
			
			$repass = $this->useradderService->useradder($data,$username);
			return $repass;
		} else {
			$result = $this->useradderService->adderall($username);
			return $result;
		}
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
