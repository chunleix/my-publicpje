<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Service\SelcartgoodService;
use Illuminate\Support\Facades\Session;
use Redis;
class SelcartgoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	private static $selcartgoodService;
    public function __construct(SelcartgoodService $selcartgoodService)
    {
        self::$selcartgoodService = $selcartgoodService;
    }
	 
	 
    public function index(Request $request,Response $response)
    {
		
		$good_id = $request->get('good_id'); 
		$num = $request->get('num'); 
		$username = \Redis::get('username');
		if($good_id == 'all'){
			$iscart = self::$selcartgoodService->allsel($username,$num);
			return $iscart;
		}
		
		
		if(empty($username)){
			return '0';
		}
		
        $iscart = self::$selcartgoodService->selcart($good_id,$username,$num);
		return $iscart;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
