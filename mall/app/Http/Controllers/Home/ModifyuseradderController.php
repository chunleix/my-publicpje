<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Service\ModifyuseradderService;
use Redis;
use DB;
class ModifyuseradderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	//private static $modifyuseradderService;
    //public function __construct(ModifyuseradderService $modifyuseradderService)
    //{
    //    self::$modifyuseradderService = $modifyuseradderService;
    //}
	 
	 
    public function index(Request $request,Response $response)
    {
		
		if($request->get('det_id')){
			$result = DB::select("select is_default from mypro_user_addr where id=?",[$request->get('det_id')]);
			foreach($result as $results){
				$is_default = $results->is_default;
			}
			
			if($is_default == 1){
				$data = array(
					'status'=>'0',
					'msg'=>'默认地址不可删除',
				);
				return $data;
			} else {
				$is = DB::delete("delete from mypro_user_addr where id=?",[$request->get('det_id')]);
				
				if($is){
					$data = array(
						'status'=>'1',
					'msg'=>'删除成功',
					);
				}
				return $data;
			}
		}
		
		$username = $request->all();
		
		if($username['is_del']){
			$result = DB::select("select * from mypro_user_addr where user_id=?",[$username['user_id']]);
			
			foreach($result as $results){
				DB::update("update mypro_user_addr set is_default=0 where user_id=?",[$username['user_id']]);
			}
			
			$is_default = 1;
		}
		
		$result = DB::update("update mypro_user_addr set province_code=?,city_code=?,area_code=?,town_code=?,detailed_addr=?,addr_name=?,addr_phone=?,is_default=? where id=?",[$username['province_code'],$username['city_code'],$username['area_code'],$username['town_code'],$username['detailed_addr'],$username['name'],$username['addr_phone'],$is_default,$username['id']]);
		return 'ok';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
