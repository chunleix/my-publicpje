Page({

  /**
   * 页面的初始数据
   */
  data: {
    userid:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
    wx.getStorage({
      key: 'userid',
      success: function (res) {
        if (res == null) {
          wx.navigateTo({
            url: '../index/index',
          })
        } else {
          wx.navigateTo({
            url: '../user/index',
          })
        }
      }
    })
  },
  formSubmit:function(e){
    wx.showLoading({
      title: '正在登陆',
      mask:true
    })
      wx.request({
        url: 'http://www.mall.com/login',
        data: {
          username: e.detail.value.username,
          password: e.detail.value.password
        },
        header: {
          'content-type': 'application/json'
        },
        success: function (res) {

          if (res.data.status == 500) {
            wx.showToast({
              title: res.data.msg,
              icon: 'fail',
              image: '../images/fail.png',
              duration: 1000,
              mask: true
            })
            return;
          }
          //将用户id缓存
          wx.setStorageSync('userid', res.data.userid);
          
          wx.showToast({
            title: res.data.msg,
            icon: 'success',
            duration: 2000,
            mask: true,
            success: function () {
              setTimeout((function callback() {
                wx.navigateTo({
                  url: '../user/index'
                })
              }).bind(this), 2000);
            }
          })

        }
      })
  },

  toRegister:function(){
    wx.navigateTo({
      url: '../register/index'
    })
  },

  

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})