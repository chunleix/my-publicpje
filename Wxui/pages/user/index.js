Page({
  data: {
    username:'',
  },
  toUserdata: function () {
    wx.redirectTo({
      url: '../userdata/index'
    })
  },
  tomodiflpass: function () {
    wx.redirectTo({
      url: '../modifypass/index'
    })
  },
  onReady: function () {
    var that = this;
    wx.getStorage({
      key: 'userid',
      success: function (res) {
        if(res == null){
          wx.redirectTo({
            url: '../index/index',
          })
        } else {
          wx.request({
            url: 'http://www.mall.com/selectuser',
            data: {
              userid: res.data
            },
            header: {
              'content-type': 'application/json'
            },
            success: function (res) {

              if (res.data.status == 200) {
                that.setData({
                  username: res.data.data.name
                })
              } else {
                console.log(res.data.msg);
              }
            }
          })
        }
      }
    })
  },
})