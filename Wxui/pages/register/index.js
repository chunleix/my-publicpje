// pages/register/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  formSubmit: function (e) {
    
    if (e.detail.value.username == null){
      wx.showToast({
        title: '请输入账号',
        icon: 'fail',
        image: '../images/fail.png',
        duration: 1000,
        mask: true
      })
      return;
    }
    if (e.detail.value.password == null) {
      wx.showToast({
        title: '请输入密码',
        image: '../images/fail.png',
        duration: 1000,
        mask: true
      })
      return;
    }
    if (e.detail.value.repass != e.detail.value.password) {
      wx.showToast({
        title: '密码不一致',
        image: '../images/fail.png',
        duration: 1000,
        mask: true
      })
      return;
    }
    wx.request({
      url: 'http://www.mall.com/adduser',
      data: {
        username: e.detail.value.username,
        password: e.detail.value.password,
        repass: e.detail.value.repass
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if (res.data.status == 500){
          wx.showToast({
            title: res.data.msg,
            icon: 'fail',
            image:'../images/fail.png',
            duration: 1000,
            mask: true
          })
          return;
        }
        wx.showToast({
          title: '注册成功',
          icon: 'success',
          duration: 2000,
          mask:true,
          success:function(){
            setTimeout((function callback() {
              wx.navigateTo({
                url: '../user/index'
              })
            }).bind(this), 2000);
          }
        })
        
      }
    })
  },
  formReset: function () {
    console.log('清空数据');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})