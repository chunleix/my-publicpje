// pages/userdata/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userlist: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.getStorage({
      key: 'userid',
      success: function (res) {
        if (res.data == null) {
          wx.navigateTo({
            url: '../index/index',
          })
        } else {
          wx.request({
            url: 'http://www.mall.com/selectuser',
            data: {
              userid: res.data
            },
            header: {
              'content-type': 'application/json'
            },
            success: function (res) {

              if (res.data.status == 200) {
                that.setData({
                  userlist: res.data.data
                })
              } else {
                console.log(res.data.msg);
              }
            }
          })
        }
      }
    })

  },

  formSubmit:function(e){
    wx.getStorage({
      key: 'userid',
      success: function (res) {
        if (res.data == null) {
          wx.navigateTo({
            url: '../index/index',
          })
        } else {
          if (e.detail.value.name == '' && e.detail.value.username == '' && e.detail.value.email == '' && e.detail.value.bumen == '') {
            wx.navigateTo({
              url: '../user/index',
            })
          } else {
            wx.request({
              url: 'http://www.mall.com/modifyuser',
              data: {
                nickname: e.detail.value.username,
                pic: e.detail.value.name,
                section: e.detail.value.bumen,
                email: e.detail.value.email,
                guid: e.detail.value.guid
              },
              header: {
                'content-type': 'application/json'
              },
              success: function (res) {
                  if(res.data.status == 200){
                    wx.showToast({
                      title: res.data.msg,
                      icon: 'success',
                      duration: 1000,
                      mask: true,
                      success: function () {
                        setTimeout((function callback() {
                          wx.navigateTo({
                            url: '../user/index'
                          })
                        }).bind(this), 2000);
                      }
                    })
                  }
              }
            })
          }
        }
      }
    })
    

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  primary: function () {
    wx.navigateTo({
      url: '../editdata/index'
    })
  }
})