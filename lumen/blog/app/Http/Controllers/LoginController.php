<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Laravel\Lumen\Routing\Controller as BaseController;

class LoginController extends BaseController
{
    //用户注册
    function AddUser(Request $request){

        $username = $request->only('username');
        $userdata['loginname'] = $username['username'];
        $password = $request->only('password');
        $repass = $request->only('repass');
        $repass = MD5($repass['repass']);
        $userdata['ip'] = $_SERVER["REMOTE_ADDR"];
        $userdata['addtime'] = date('Y-m-d H:i:s',time());
        $userdata['lasttime'] = date('Y-m-d H:i:s',time());

        if(empty($userdata['loginname'])){
            $data = array(
                'status'=>500,
                'msg'=>'请输入用户名'
            );
            return $data;
        }

        if($password['password'] == ''){
            $data = array(
                'status'=>500,
                'msg'=>'请输入密码'
            );
            return $data;
        } else {

            $userdata["password"] = MD5($password['password']);
        }
        if($repass != $userdata["password"]){
            $data = array(
                'status'=>500,
                'msg'=>'密码不一致'
            );
            return $data;
        }

        $existence = DB::table('data_login')->where('loginname',$userdata['loginname'])->first();

        if(!empty($existence)){
            $data = array(
                'status'=>500,
                'msg'=>'用户已存在'
            );
            return $data;
        }

        //添加用户到用户表
        $id = DB::table('data_login')->insertGetId($userdata);
        //添加用户信息到用户信息表
        $userinfo['guid'] = $id;
        $userinfo['nickname'] = $userdata['loginname'];
        $userinfo['pic'] = '默认名字';
        $userinfo['section'] = '未填写';
        $userinfo['email'] = '未填写';

        $dataid = DB::table('data_user_info')->insertGetId($userinfo);

        //添加用户登陆日志，到登录日志表
        $userlog['guid'] = $id;
        $userlog['logintime'] = $userdata['addtime'];
        $userlog['ip'] = $userdata['ip'];

        $log = DB::table('log_login')->insertGetId($userlog);

        //$result = DB::insert("INSERT INTO data_login(loginname,password,ip,addtime,lasttime,status) VALUES('{$username['username']}','{$password}','{$ip}','{$addtime}','{$addtime}',{$status})");
        if($id){
            $data = array(
                'status'=>200,
                'msg'=>'添加成功',
            );
        } else {
            $data = array(
                'status'=>500,
                'msg'=>'添加失败',
            );
        }
        return $data;
    }
    //用户登陆
    public function Login(Request $request){
        //session()

        $username = $request->only('username');
        $password = $request->only('password');

        if(empty($username['username'])){
            $data = array(
                'status'=>500,
                'msg'=>'请输入用户名'
            );
            return $data;
        }
        if($password['password'] == ''){
            $data = array(
                'status'=>500,
                'msg'=>'请输入密码'
            );
            return $data;
        }

        $userdata['pass'] = MD5($password['password']);

        $result = DB::table('data_login')->where('loginname',$username['username'])->first();

        if(!empty($result)){
            $result = DB::table('data_login')->where('loginname',$username['username'])->where('password',$userdata['pass'])->first();

            if(!empty($result)){

                //如果登陆成功，则保存登陆信息
                $userlog['guid'] = $result->id;
                $userlog['logintime'] = date('Y-m-d H:i:s',time());
                $userlog['ip'] = $_SERVER["REMOTE_ADDR"];
                $result = DB::table('log_login')->where('guid',$userlog['guid'])->first();
                //如果存在，则修改登陆信息
                if($result){
                    DB::update('update log_login set logintime = ?, ip = ? where guid = ?',[$userlog['logintime'],$userlog['ip'],$userlog['guid']]);
                    DB::update('update data_login set lasttime = ? where id = ?',[$userlog['logintime'],$userlog['guid']]);
                } else {
                    //如果不存在则，添加登陆信息
                    $id = DB::table('log_login')->insertGetId($userlog);
                }
                $data = array(
                    'status'=>'200',
                    'msg'=>'登陆成功',
                    'userid'=>$userlog['guid']
                );
            } else {
                $data = array(
                    'status'=>'500',
                    'msg'=>'密码错误'
                );
            }
            return $data;
        } else {
            $data = array(
                'status'=>'500',
                'msg'=>'没有此用户'
            );
            return $data;
        }
        return $result;

    }

    //用户资料查询
    public function SelectUser(Request $request){
        $user_id = $request->only('userid');
        $result = DB::table('data_user_info')->where('guid',$user_id['userid'])->first();
        if(!empty($result)){
            $data = array(
                'status'=>'200',
                'data'=>array(
                    'id'=>$result->id,
                    'guid'=>$result->guid,
                    'username'=>$result->nickname,
                    'name'=>$result->pic,
                    'bumen'=>$result->section,
                    'email'=>$result->email
                )
            );
        } else {
            $data = array(
                'status'=>'500',
                'msg'=>'请求数据失败'
            );
        }
        return $data;
    }
    //修改用户资料
    public function ModifyUser(Request $request){
        $nickname = $request->only('nickname');
        $pic = $request->only('pic');
        $section = $request->only('section');
        $email = $request->only('email');
        $guid = $request->only('guid');
        $request = DB::update('update data_user_info set nickname = ?, pic = ?, section = ?, email = ? where guid = ?',[$nickname['nickname'],$pic['pic'],$section['section'],$email['email'],$guid['guid']]);
        //$sql = "update data_user_info set nickname = {$nickname['nickname']}, pic = {$pic['pic']}, section = {$section['section']}, email = {$email['email']} where guid = {$guid['guid']}";
        $data = array(
            'status'=>'200',
            'msg'=>'修改成功'
        );
        return $data;
    }
}