import foo from './src/foo.vue'
import product from './src/product.vue'
import login from './src/login.vue'
import shopcart from './src/shopcart.vue'
import personal from './src/personal.vue'
import app from './src/app.vue'


import VueRouter from 'vue-router'

Vue.use(VueRouter)
const routes = [
	{path:'/',component:foo},
	{path:'/product',component:product},
	{path:'/login',component:login},
	{path:'/shopcart',component:shopcart},
	{path:'/personal',component:personal},
]

const router = new VueRouter({
	routes
})
app.el = '#root'
app.router = router
export default new Vue(app);
